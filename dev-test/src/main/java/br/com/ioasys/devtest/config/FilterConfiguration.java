package br.com.ioasys.devtest.config;

import br.com.ioasys.devtest.models.enums.ProfileEnum;
import br.com.ioasys.devtest.security.UserSS;
import br.com.ioasys.devtest.security.UserSSResources;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter
public class FilterConfiguration implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.setHeader("Content-Type","application/json");
        UserSS user = UserSSResources.authtenticated();
        if(user == null)
            httpServletResponse.setHeader("Role","none");
        else
            httpServletResponse.setHeader("Role", getType(user));
        chain.doFilter(request,response);
    }

    private String getType(UserSS user){
        if (user.hasRole(ProfileEnum.ROLE_ADMIN))
            return "ADMIN";
        return "USER";
    }
}
