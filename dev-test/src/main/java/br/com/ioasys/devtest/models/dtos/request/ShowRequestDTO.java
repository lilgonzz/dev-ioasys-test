package br.com.ioasys.devtest.models.dtos.request;

import javax.persistence.Column;
import java.time.LocalDate;
import java.util.List;

public class ShowRequestDTO {

    private String gender;
    private String release;
    private String directorName;
    private String name;
    private List<String> actors;

    public ShowRequestDTO(){}
    public ShowRequestDTO(String gender, String release, String directorName, String name, List<String> actors) {
        this.gender = gender;
        this.release = release;
        this.directorName = directorName;
        this.name = name;
        this.actors = actors;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }
}
