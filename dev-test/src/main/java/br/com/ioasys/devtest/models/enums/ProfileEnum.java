package br.com.ioasys.devtest.models.enums;

public enum ProfileEnum {
    ROLE_ADMIN,
    ROLE_USER
}
