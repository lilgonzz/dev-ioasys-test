package br.com.ioasys.devtest.controller;

import br.com.ioasys.devtest.models.User;
import br.com.ioasys.devtest.models.dtos.request.UserRequestDTO;
import br.com.ioasys.devtest.models.dtos.response.UserResponseDTO;
import br.com.ioasys.devtest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;
    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("admin/search")
    public ResponseEntity<?> getAllUsers(@RequestParam(name = "page", defaultValue = "false") Boolean page,
                                         @RequestParam(value = "pageNumber", defaultValue = "0") Integer total,
                                         @RequestParam(value = "linesPerPage", defaultValue = "24") Integer lines){

        if(page == false){
            List<UserResponseDTO> users = service.getAll();
            return ResponseEntity.ok(users);
        }

        Page<User> list = service.getPage(total, lines);
        Page<UserResponseDTO> response = list.map(obj -> new UserResponseDTO(obj));
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/post")
    public ResponseEntity<Void> insert(@RequestBody UserRequestDTO userDto){

        User nUser = service.convertDto(userDto);
        nUser = service.postNewUser(nUser);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(nUser.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }
    @PreAuthorize("hasAnyRole('USER')")
    @PutMapping("/put/{id}")
    public ResponseEntity<Void> update(@RequestBody UserRequestDTO userDto, @PathVariable Integer id) throws Exception {
        User nUser = service.convertDto(userDto);
        nUser.setId(id);
        nUser = service.updateUser(nUser);
        if(nUser == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.noContent().build();
    }
    @PreAuthorize("hasAnyRole('USER')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) throws Exception {
        User user = service.deleteUser(id);
        if(user == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.noContent().build();
    }
    @PreAuthorize("hasAnyRole('ADMIN')")
    @PatchMapping("admin/setAdmin/{id}")
    public ResponseEntity<Void> updateAdmin(@PathVariable Integer id) throws Exception {
        User user = service.updateAdm(id);
        if(user == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.noContent().build();
    }
}
