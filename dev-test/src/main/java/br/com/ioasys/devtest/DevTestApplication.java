package br.com.ioasys.devtest;

import br.com.ioasys.devtest.models.User;
import br.com.ioasys.devtest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class DevTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(DevTestApplication.class, args);

	}

}
