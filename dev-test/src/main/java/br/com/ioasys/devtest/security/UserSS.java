package br.com.ioasys.devtest.security;

import br.com.ioasys.devtest.models.enums.ProfileEnum;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

public class UserSS implements UserDetails {

    private Integer id;
    private String email;
    private String senha;
    private Collection<? extends GrantedAuthority> authorities;

    public UserSS(){}
    public UserSS(Integer id, String email, String senha, Set<ProfileEnum> profile){
        super();
        this.id = id;
        this.email = email;
        this.senha = senha;
        this.authorities = profile.stream().map(x -> new SimpleGrantedAuthority(getDescription(x))).collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Integer getId(){
        return id;
    }
    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    private String getDescription(ProfileEnum x){
        String str = (x == ProfileEnum.ROLE_ADMIN) ? "ROLE_ADMIN" : "ROLE_USER";
        return str;
    }

    public boolean hasRole(ProfileEnum role) {

        return getAuthorities().contains(new SimpleGrantedAuthority(role.toString()));
    }
}
