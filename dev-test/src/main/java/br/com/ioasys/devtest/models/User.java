package br.com.ioasys.devtest.models;

import br.com.ioasys.devtest.models.enums.ProfileEnum;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
public class User extends BaseClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name="password")
    private String password;
    @Column(name = "email")
    private String email;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable
    private Set<ProfileEnum> profile = new HashSet<>();
    public User(){}

    public User(String name, String password, String email, Boolean admin) {
        super(name,true,LocalDateTime.now(),null);
        if(password != null)
            this.password = encodePassword(password);
        this.email = email;
        this.profile.add(ProfileEnum.ROLE_USER);
        if(admin == true){
            this.profile.add(ProfileEnum.ROLE_ADMIN);
        }

    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id){ this.id = id; }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = encodePassword(password);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String encodePassword(String password){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    public Set<ProfileEnum> getProfile(){
        return this.profile;
    }

    public void addProfile(ProfileEnum profile){
        this.profile.add(profile);
    }
}
