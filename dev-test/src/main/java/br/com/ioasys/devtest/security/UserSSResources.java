package br.com.ioasys.devtest.security;

import org.springframework.security.core.context.SecurityContextHolder;

public class UserSSResources {

    public static UserSS authtenticated(){
        try{
            return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch(Exception e){
            return null;
        }
    }
}
