package br.com.ioasys.devtest.repositories;

import br.com.ioasys.devtest.models.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {

    @Query(value = "SELECT * FROM ACTORS WHERE NAME = ?1", nativeQuery = true)
    public Actor findByName(String name);
}
