package br.com.ioasys.devtest.controller;

import br.com.ioasys.devtest.models.Show;
import br.com.ioasys.devtest.models.User;

import br.com.ioasys.devtest.models.dtos.request.ShowRequestDTO;
import br.com.ioasys.devtest.models.dtos.request.VoteRequestDTO;
import br.com.ioasys.devtest.models.dtos.response.ShowResponseDTO;
import br.com.ioasys.devtest.models.dtos.response.UserResponseDTO;
import br.com.ioasys.devtest.service.ShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/show")
public class ShowController {

    @Autowired
    private ShowService service;

    @GetMapping("get/{id}")
    public ResponseEntity<ShowResponseDTO> getShow(@PathVariable Integer id) throws Exception {
        ShowResponseDTO show = service.getShowById(id);
        if(show == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok().body(show);
    }

    @GetMapping("/search")
    public ResponseEntity<?> getAllShows(@RequestParam(name = "page", defaultValue = "false") Boolean page,
                                         @RequestParam(value = "pageNumber", defaultValue = "0") Integer total,
                                         @RequestParam(value = "linesPerPage", defaultValue = "24") Integer lines,
                                         @RequestParam(value = "filter", defaultValue = "none") String filter){

        if(page == false){
            List<ShowResponseDTO> shows = service.getAll();
            return ResponseEntity.ok(shows);
        }
        Page<ShowResponseDTO> list = service.getPage(total, lines, filter);
        return null;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/post")
    public ResponseEntity<Void> createShow(@RequestBody ShowRequestDTO show){
       Show nShow = service.create(show);
       URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(nShow.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }
    @PreAuthorize("hasAnyRole('USER')")
    @PutMapping("/vote/{id}")
    public ResponseEntity<String> vote(@PathVariable Integer id, @RequestBody VoteRequestDTO vote) throws Exception {
        Show show = service.voteShow(id,vote.getVote());
        if(show == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.noContent().build();
    }
}
