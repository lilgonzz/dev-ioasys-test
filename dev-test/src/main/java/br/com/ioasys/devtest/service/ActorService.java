package br.com.ioasys.devtest.service;


import br.com.ioasys.devtest.models.Actor;
import br.com.ioasys.devtest.models.Show;
import br.com.ioasys.devtest.models.dtos.request.ShowRequestDTO;
import br.com.ioasys.devtest.repositories.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActorService {

    @Autowired
    private ActorRepository repository;

    public Show createActor(Show show, ShowRequestDTO dto){
        List<Actor> list = new ArrayList<Actor>();
        for(String name : dto.getActors()){
            Actor actor= repository.findByName(name);
            if(actor == null)
                actor = new Actor(name);
            actor.setShow(show);
            repository.save(actor);
            list.add(actor);
        }
        show.setActors(list);
        return show;
    }
}
