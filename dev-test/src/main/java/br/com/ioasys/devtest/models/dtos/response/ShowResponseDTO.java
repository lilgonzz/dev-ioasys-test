package br.com.ioasys.devtest.models.dtos.response;

import br.com.ioasys.devtest.models.Actor;
import br.com.ioasys.devtest.models.Show;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.stream.Collectors;

public class ShowResponseDTO implements Serializable {

    private String name;
    private String gender;
    private String directorName;
    private String release;
    private double totalStar;
    private List<String> nameActors;

    public ShowResponseDTO(){}
    public ShowResponseDTO(Show show){
        this.name = show.getName();
        this.gender = show.getGender();
        this.directorName = show.getDirectorName();
        this.release = show.getRelease().toString();
        this.totalStar = mediaStar(show.getTotalStars(), show.getTotalVotes());
        this.nameActors = namesFromActors(show.getActors());
    }

    private double mediaStar(Integer stars, Integer votes){
        if (votes == 0)
            return 0;
        DecimalFormat df = new DecimalFormat("#.##");
        double star = stars;
        return Double.parseDouble(df.format(star/votes).replace(',','.'));
    }

    private List<String> namesFromActors(List<Actor> list){
        return list.stream().map(obj -> obj.getName()).collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public double getTotalStar() {
        return totalStar;
    }

    public void setTotalStar(double totalStar) {
        this.totalStar = totalStar;
    }

    public List<String> getNameActors() {
        return nameActors;
    }

    public void setNameActors(List<String> nameActors) {
        this.nameActors = nameActors;
    }
}
