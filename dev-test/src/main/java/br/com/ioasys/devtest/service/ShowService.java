package br.com.ioasys.devtest.service;

import br.com.ioasys.devtest.models.Show;
import br.com.ioasys.devtest.models.dtos.request.ShowRequestDTO;

import br.com.ioasys.devtest.models.dtos.response.ShowResponseDTO;
import br.com.ioasys.devtest.repositories.ShowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ShowService {

    @Autowired
    private ShowRepository repository;
    @Autowired
    private ActorService actorService;

    public Page<ShowResponseDTO> getPage(Integer total, Integer lines, String filter){
            Pageable pageable = PageRequest.of(total, lines, Sort.Direction.ASC, filter);
            Page<Show> page;
            if(!filter.equals("none")) {
                page = repository.findAll(pageable);
            }
            else{
                page = repository.findAll(pageable);
            }
            Page<ShowResponseDTO> newPage = page.map(obj -> new ShowResponseDTO(obj));
            return newPage;
    }
    public List<ShowResponseDTO> getAll(){
        List<Show> list = repository.findAll();
        list = sort(list);
        List<ShowResponseDTO> dto = list.stream().map(obj -> new ShowResponseDTO(obj)).collect(Collectors.toList());
        return dto;
    }
    public Show create(ShowRequestDTO dto){
        Show show = fromDTO(dto);
        show = actorService.createActor(show, dto);
        show.setId(null);
        return repository.save(show);
    }
    public Show voteShow(Integer id, Integer vote) throws Exception {
        Optional<Show> show = repository.findById(id);
        if(!checkValidShow(show)){
            return null;
        }
        Show nShow = show.get();
        if (vote > 4)
            vote = 4;
        if (vote < 0)
            vote = 0;

        nShow.setTotalStars(vote);
        nShow.addVote();
        return repository.save(nShow);
    }

    public ShowResponseDTO getShowById(Integer id) throws Exception {
        Optional<Show> nShow = repository.findById(id);
        if(!checkValidShow(nShow))
            return null;

        return new ShowResponseDTO(nShow.get());
    }
    public Show fromDTO(ShowRequestDTO dto){
        return new Show(dto);
    }
    private List<Show> sort(List list){
        Comparator<Show> compare = Comparator.comparing(Show::getTotalVotes, (s1,s2) ->{ return s2.compareTo(s1); });
        compare = compare.thenComparing(Comparator.comparing(Show::getName, (s1,s2) -> {return s1.compareTo(s2); }));
        Collections.sort(list, compare);
        return list;
    }

    private Boolean checkValidShow(Optional<Show> show){
        if(show.isPresent())
            return true;
        return false;
    }
}
