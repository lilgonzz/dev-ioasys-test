package br.com.ioasys.devtest.models.dtos.request;

public class VoteRequestDTO {
    private Integer vote;

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }
}
