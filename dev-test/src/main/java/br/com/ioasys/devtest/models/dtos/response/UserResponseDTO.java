package br.com.ioasys.devtest.models.dtos.response;

import br.com.ioasys.devtest.models.User;

public class UserResponseDTO {
    private String nome;
    private String email;

    public UserResponseDTO(){}
    public UserResponseDTO(User user){
        this.nome = user.getName();
        this.email= user.getEmail();
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
