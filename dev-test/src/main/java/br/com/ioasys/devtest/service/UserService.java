package br.com.ioasys.devtest.service;

import br.com.ioasys.devtest.models.Show;
import br.com.ioasys.devtest.models.User;
import br.com.ioasys.devtest.models.dtos.request.UserRequestDTO;
import br.com.ioasys.devtest.models.dtos.response.UserResponseDTO;
import br.com.ioasys.devtest.models.enums.ProfileEnum;
import br.com.ioasys.devtest.repositories.UserRepository;
import br.com.ioasys.devtest.security.UserSS;
import br.com.ioasys.devtest.security.UserSSResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public List<UserResponseDTO> getAll(){
        List<User> users = repository.findAllActive();
        users = findNonAdmin(users);
        List<UserResponseDTO> dtos = users.stream().map(obj -> new UserResponseDTO(obj)).collect(Collectors.toList());
        return dtos;
    }

    public User postNewUser(User user){
        user.setId(null);
        return repository.save(user);
    }
    public Page<User> getPage(Integer total, Integer lines){
        PageRequest request = PageRequest.of(total, lines, Sort.Direction.ASC, "name");
        return repository.findAll(request);
    }
    public User updateUser(User user) throws Exception {
        User nUser = find(user.getId());
        if (user == null)
            return null;
        updateData(nUser, user);
        return repository.save(nUser);
    }
    public User updateAdm(Integer id) throws Exception {
        User user = find(id);
        if (user == null)
            return null;
        user.addProfile(ProfileEnum.ROLE_ADMIN);
        return repository.save(user);
    }
    public User deleteUser(Integer id) throws Exception {
        User user = find(id);
        if (user == null)
            return null;

        user.setActive(false);
        return repository.save(user);
    }
    public User convertDto(UserRequestDTO user){
        return new User(user.getName(), user.getPassword(), user.getEmail(), user.getAdmin());
    }

    private User find(Integer id) throws Exception {
        UserSS user = UserSSResources.authtenticated();
        if(user==null || !user.hasRole(ProfileEnum.ROLE_ADMIN) && !id.equals(user.getId()))
        {
            throw new Exception("not authorized");
        }
        Optional<User> newUser = repository.findById(id);
            if(!checkValidUser(newUser))
                return null;
        return newUser.get();
    }
    private void updateData(User nUser, User user){
        nUser.setName((user.getName() == null) ? nUser.getName() : user.getName());
        nUser.setEmail((user.getEmail() == null) ? nUser.getEmail() : user.getEmail());
        nUser.setPassword((user.getPassword() == null) ? nUser.getPassword() : user.getPassword());
    }


    private Boolean checkValidUser(Optional<User> user){
        if(user.isPresent())
            return true;
        return false;
    }
    private List<User> findNonAdmin(List<User> users) {
        List<User> list = new ArrayList<>();
        for(User user : users){
            if (!user.getProfile().contains(ProfileEnum.ROLE_ADMIN));
                list.add(user);
        }

        return list;
    }
}
