package br.com.ioasys.devtest.models;

import br.com.ioasys.devtest.models.dtos.request.ShowRequestDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

@Entity
@Table(name = "shows")
public class Show extends BaseClass{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "gender")
    private String gender;
    @Column(name = "date_release")
    private LocalDate release;
    @Column(name = "director_name")
    private String directorName;
    @Column(name = "total_stars")
    private Integer totalStars;
    @Column(name = "total_votes")
    private Integer totalVotes;
    @ManyToMany
    @JoinTable(
            name = "actors_shows",
            joinColumns = @JoinColumn(name = "show_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id"))
    private List<Actor> actors = new ArrayList<Actor>();

    public Show() {
    }

    public Show(ShowRequestDTO dto) {
        super(dto.getName(), true, LocalDateTime.now(), null);
        this.gender = dto.getGender();
        this.release = convertToDate(dto.getRelease());
        this.directorName = dto.getDirectorName();
        this.totalStars = 0;
        this.totalVotes = 0;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getRelease() {
        return release;
    }

    public void setRelease(LocalDate release) {
        this.release = release;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public Integer getTotalStars() {
        return totalStars;
    }

    public void setTotalStars(Integer totalStars) {
        this.totalStars += totalStars;
    }

    public Integer getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(Integer totalVotes) {
        this.totalVotes = totalVotes;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
    public void addVote(){
        this.totalVotes += 1;
    }
    private LocalDate convertToDate(String release) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try{
            return LocalDate.parse(release, formatter);
        } catch (Exception e){
            System.out.println("not possible");
            return null;
        }
    }

}
