package br.com.ioasys.devtest.repositories;

import br.com.ioasys.devtest.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query(value = "SELECT * FROM USERS WHERE IS_ACTIVE = 1", nativeQuery = true)
    public List<User> findAllActive();
    @Query(value = "SELECT * FROM USERS WHERE EMAIL = ?1", nativeQuery = true)
    public User findByEmail(String email);
}
